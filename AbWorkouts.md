# Tia's Ab Workouts

## 1. ABS WORKOUTS FROM TIA – CORE STRENGTHENER

4 Rounds. Rest 1min between each round or partner up and do Round for Round.

- 30 sec Hollow Rock Hold
- 5 V-Ups
- 10 Hollow Rocks

https://www.instagram.com/p/BTXl8KXDNI4/?taken-by=tiaclair1

## 2. CORE WOD

Every 90 sec, 4 rounds:

- 6 Around the world with plate above head (15kg, 3ea)
- 20 Russian Twists and then hold the Hollow Rock position for 20-30sec
- *Goal is to maximise the amount of time spent in the hollow rock position

## 3. THREE ROUND TABATA WORKOUT

You can do it anywhere with no equipment needed just a timer from your phone. https://www.instagram.com/p/BSsGTRijq0L/?utm_source=ig_web_button_share_sheet

3 Rounds: (TABATA STYLE; 20secs on: 10secs off)

- 8 Fast V-Ups
- 15 Tuck Crunches
- 30 sec Hollow Rocks
- 30 sec Superman
- Then Rest 1 min into Max Bridge Hold.
- *be sure to use the rest between the crunches and hollow rocks because those 30secs get really hard.

## 4. ABS WORKOUTS – PLANK WOD

https://www.instagram.com/p/BRksTczDcvO/?utm_source=ig_web_button_share_sheet

- Accumulate 6mins of the following in 8mins;
- 2min Plank hold
- 2min Side Plank (each side, total 4mins)
- *I tried to challenge myself by adding weight and doing it on the boxes but for beginners this can be performed on the ground.

## 5. ABS WORKOUT – TABATA CORE WOD

Here we go guys…..Try this one! ??? You will definitely have a stronger core after…

20 secs ON: 10 secs OFF

– Around the World (Acc 50reps)  
– AB Mat Sit-Ups (10/12 reps)

Maintain 10/12 AB Mat Sit-Ups on every 2nd set of Tabata, while trying to continue to accumulate 50reps of Around the world ? on the opposite round of Tabata.

## 6. SQUAT

https://www.instagram.com/p/BSQbcQeDX6J/?utm_source=ig_web_button_share_sheet

Almost every athletic movement you can think of involves the posterior chain one way or another, whether it be a jump, a side step, a leap or a deceleration. Some researchers have suggested that developing your posterior chain will also improve horizontal power and top end speed; these are two of the most important attributes to have in almost any sport. And how do we improve that, simple… Deadlift and Squat.

## 7. T2B WINDOW WIPERS

https://www.instagram.com/p/BPHhNnag8D2/?utm_source=ig_web_button_share_sheet

I love watching people perform gymnastics movements that require a lot of body control and skill…..so today I thought I would give it a try and clearly it’s something I need to work on. ? Tag a friend you know that can do this.

# Kari Pierce Workout EMOMs - Level 2

## Day 1: 2 rounds, :40 on / :20 off

1. Mountain Climbers

2. Hollow Hold

3. Plank Walk Ups

4. Supermans

5. Windshield Wipers

## Day 2: 9 minute AMRAP

1. 10 Burpees

2. 10 V-ups

3. 10 Plank Walkouts

4. 10 Reach throughs

5. 10 Seated leg lifts

## Day 3: 3 rounds, :30 on / :15 off

1. Rotating Planks

2. V-sit Slides

3. Crossbody Mountain Climbers

4. Candlestick to Jump

## Day 4: 2 rounds, 1 minute each

1. Flutter kicks

2. Wall Walks

3. Tap Crunches

4. Floor thrusters with Push-up

5. Rolling V-up

## Day 5: 3 rounds, :30 each

1. Hollow Rocks

2. Arch Rocks

3. Rest 1:00

4. 6 MIN AMRAP
   1. 6 Burpees
   2. 12 Reverse Crunches with a twist (6 each)
   3. 18 Walking V-ups (9 each)
