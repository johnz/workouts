# Daily Workout

#### Daily Posture Work at Desk - [How to fix your POSTURE in 10 Minutes (Prevent Injury, Realign your Spine, Perform Better) - YouTube](https://youtu.be/urGJm1XwFsQ)

**2 rounds x 1 minute of each movement (from the video above**) 

- Crouching chest stretch

- Chin tuck - bring chin back

- Reverse fly - bend over, pinch traps, bring arms back

- Lunge with side lean - 1 min each side

- Plank with chin tuck

- Plantar stretch

#### Daily Warmup

Moderate pace

* 10 Jumping jax
* 10 Side-to-side jax
* 10 Inchworms
* 10 Hip thrusters / glute bridges
* 10 Goodmornings
* 10 Knee hugs
* 30s Primal squat
* 10 x 5 Seconds Weighted superman
* 120 Jump rope singles
* Front rack mobility - 30 secs per arm

#### For Strength

* Bear Complex: 1 Power Clean, 1 Front Squat, 1 Push Press, 1 Back Squat, 1 Back Rack Push Press as individual movements

#### Metcon 1

3 rounds for time

* 50 rolling situps
* 30 push ups
* 30 toes to wall/rig
* 30 dumbell thrusters
* 20 weighted lunges
* 2 minutes plank, choice
* 10 Man makers, or scale to renegade rows
* Stretching

#### Metcon 2

3 rounds, focusing on form

* 20 Roll-up situps
* 20 Push ups
* 20 Air/light weight squats
* 10 Pull ups
* 30 secs side plank each side
* 10 Front lunges, with weight
* 10 Dumbell shoulder press
* 60 Seconds front plank
* 2000 m row
