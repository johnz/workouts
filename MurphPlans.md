# Murph Plans

## 4 Week Program

**The Workout:**

- 1-Mile Run

- 100 Pull-ups

- 200 Push-ups

- 300 Squats

- 1-Mile Run

**Prescribed with a 20lb weighted vest*

#### To compete: do 20x 5 pull-ups, 10 push-ups and 15 squats, each of the 20 in 1 minute 30 seconds.

from [https://missioncrossfitsa.com/4-week-program-crush-murph/](https://missioncrossfitsa.com/4-week-program-crush-murph/)

* A 10:00 Mile is a comfortable pace for the majority clocking in at 2 minutes and 30 seconds per 400m.  Keep this pace and you’re sure to not be out of breath when you get to the Pull-ups, Push-ups and Squats.  The repetitions can be partitioned any way you see fit.  I recommend breaking the repetitions in to 20 rounds of 5 pull-ups, 10 push-ups and 15 squats.  This allows you to not burn out on any one particular movement, hopefully not tear your hands and keep yourself moving the entire time.

* Breaking it down into 20 rounds in 30:00 allows you to complete 1 round (5 pull-ups, 10 push-up and 15 air squats) in 1:30.  that is 1 repetition performed every :03 seconds.  Most people will be able to perform the push-ups and squats much easier than the pull-ups, so if that is you, make sure to keep your pace strong during those movements to allow for some more time at the pull-ups.

Each sequence is completed with a :90 clock on a cycle.  When you’ve completed the given amount of repetitions for that sequence, rest the remainder of :90.   Complete 1 round like we talked about above (pull-ups, push-ups, then squats) with the given repetitions in :90.  You can absolutely cut this window of time down if you’d like, but I have based this off a comfortable working pattern.

Example: 

Week 1: Monday – broken down like this:

* 3 pull-ups + 6 push-ups + 8 squats in :90

* 3 pull-ups + 6 push-ups + 8 squats in :90

* 3 pull-ups + 6 push-ups + 8 squats in :90

* 3 pull-ups + 6 push-ups + 8 squats in :90
  
  repeat the above for a second round. 

**WEEK 1**

Monday

- Pull-ups: 2 x (3-3-3-3)
- Push-ups: 2 x (6-6-6-6)
- Squats: 2 x (8-8-8-8)

Wednesday – Same as Monday, but increase an additional sequence of the ladder

- Pull-ups: 2 x (3-3-3-3-3)
- Push-ups: 2 x (6-6-6-6-6)
- Squats: 2 x (8-8-8-8-8)

Friday – Same as Monday, but dropped the rounds to only one, not two.

- Pull-ups: 1 x (4-3-3-3-3)
- Push-ups: 1 x (8-6-6-6-6)
- Squats: 1 x (12-8-8-8-8)

**WEEK 2**

Monday – Same as last week, but increasing an additional sequence of the ladder

- Pull-ups: 2 x (4-4-3-3-3)
- Push-ups: 2 x (8-8-6-6-6)
- Squats: 2 x (12-12-8-8-8)

Wednesday – Same as last week, but increasing an additional sequence of the ladder

- Pull-ups: 2 x (4-4-4-3-3)
- Push-ups: 2 x (8-8-8-6-6)
- Squats: 2 x (12-12-12-8-8)

Friday – Same as last week, but increasing an additional sequence of the ladder

- Pull-ups: 1 x (4-4-4-4-3)
- Push-ups: 1 x (8-8-8-8-6)
- Squats: 1 x (12-12-12-12-8)

**WEEK 3**

Monday – Same as last week, but increasing an additional sequence of the ladder

- Pull-ups: 2 x (5-4-4-4-4)
- Push-ups: 2 x (10-8-8-8-8)
- Squats: 2 x (15-12-12-12-12)

Wednesday – Same as last week, but increasing an additional sequence of the ladder

- Pull-ups: 2 x (5-5-4-4-4)
- Push-ups: 2 x (10-10-8-8-8)
- Squats: 2 x (15-15-12-12-12)

Friday – Same as last week, but increasing an additional sequence of the ladder

- Pull-ups: 1 x (5-5-5-4-4)
- Push-ups: 1 x (10-10-10-8-8)
- Squats: 1 x (15-15-15-12-12)

**WEEK 4**

Monday – Same as last week, but increasing an additional sequence of the ladder

- Pull-ups: 3 x (4-4-4-4)
- Push-ups: 3 x (8-8-8-8)
- Squats: 3 x (12-12-12-12)

Wednesday – Same as last week, but increasing an additional sequence of the ladder

- Pull-ups: 2 x (5-5-5-5-5)
- Push-ups: 2 x (10-10-10-10-10)
- Squats: 2 x (15-15-15-15-15)

Friday – Same as last week, but increasing an additional sequence of the ladder

- Pull-ups: (6 sets of 3)
- Push-ups: (6 sets of 6)
- Squats: (6 sets of 10)

**Monday, May 28** – “[Murph](https://missioncrossfitsa.com/event/memorial-day-murph-2/)”
