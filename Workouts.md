# Working out with CrossFit/HIIT

## Terms

* **AMRAP** "as many reps as possible" or "as many rounds as possible." It's a workout structure frequently used for conditioning that pushes your body to the max within a set period of time (anywhere from 3 minutes to 60 minutes).

* **Ass to grass/ground (ATG)**: ATG ensures you’re getting as low as possible when doing front, back, or air squats.

* **Chipper**: Everyone who does CrossFit is in a chipper mood all the time—oh wait, that’s not right. “Chipper” refers to a WOD that you have to chip away at in order to finish. It consists of a series of multiple movements (usually 5 to 10) where each athlete is trying to finish the entire thing as fast as humanly possible.

* **Crossfit**: The magic is in the movements. Workouts are different every day and modified to help each athlete achieve his or her goals. CrossFit workouts can be adapted for people at any age and level of fitness. CONSTANT VARIATION - Different every day, CrossFit workouts are tailored to the individual. FUNCTIONAL MOVEMENTS - CrossFit uses safe, effective and efficient movements similar to those you use every day in life. HIGH INTENSITY - In CrossFit workouts, intensity levels are adjusted to challenge the individual and safely improve fitness.

* **Double under (DU)**: While jumping rope, allow the rope to pass under your feet twice while you’re still in the air. As an alternative, you can do three single unders for every DU that is required (e.g. 20 DUs = 60 SUs).

* **EMOM** Every Minute On the Minute is a type of interval workout where you perform a specific task at the start of every minute for a set amount of time.

* **KBS**: kettle bell swing

* **Kipping**: Kipping implies the use of explosive strength in order to gain momentum when performing pullups, hand stands, pushups, and dips. For example, kipping pullups are completed without dropping from the bar. This modality starts on the pullup bar with a powerful hip drive, explosive kick, and strong pull from the arms in order to create enough momentum to get your chin up over the bar.

* **Metcon** Metabolic Conditioning is a popular term used to describe a workout involving repeated and/or sustained high-intensity exercises, usually involving weight lifting movements, with short rest periods in order to burn fat or create a “conditioning” effect

* **Pood**: Sounds gross, but a pood is actually a Russian unit of measurement used for kettlebells. One pood =16 kg/35 lbs; 1.5 pood = 24 kg/53 lbs; 2 pood = 32 kg/71 lbs

* **PR’d**: You’ll hear “PR” when an athlete achieves his or her personal record on a lift.

* **Pistol**: No, this has nothing to do with gun control, so everyone please relax. A pistol is a one-legged squat, which helps to isolate each leg and build lower-body strength.

* **RX**: When a WOD is performed RX’d, that means the athlete performs all modalities using the prescribed weight and reps. In CrossFit, all WODs can be scaled down to meet your fitness level, but the goal is to get to a place where the RX is challenging, yet doable.

* **SMR** stands for self-myofascial release, a method of eliminating/easing trigger points and restoring tissue integrity and normal function.

* **Snatch**: Get your minds out of the gutter. The snatch has many variations (power, hang, muscle), but the overall goal is to use a wide grip to lift a barbell from the floor to an overhead position in one fluid and lightening-fast motion. Tip: Keeping the bar closer to your body when hoisting it up allows for better balance.

* **Tabata**: Tabata is a work-rest method associated with many CrossFit WODs. Here’s an example: For 20 seconds, complete as many reps of a given exercise (situps, pullups, pushups, etc.) as possible. Then rest for 10 seconds and repeat this seven more times for a total of eight intervals. After your four minutes are up, your score is the least number of reps for any of the eight intervals.

* **Thruster**: Thrusters feel like the bane of all CrossFitters’ existence, especially during “Fran” Opens a New Window. when they’re done 45 times with complementing pullups. To do this move, grab a barbell and start in front rack position—standing position with the bar resting against the front of your shoulders; hold the barbell with a hook grip. Drop into a full squat position while keeping the barbell at shoulder level. Return to standing position in an explosive (thrusting) motion and push the weight up over your head. Bring the barbell back down to your shoulders and repeat.

* **WOD** stands for Workout of the Day. Plain and simple, this is the set of modalities that your coach uses to put you through hell on any given day.

* **WYLO**: Where you left off

---

# Introductory Movements - Foundations

* [CrossFit 9 Fundamental Movements](https://www.youtube.com/playlist?list=PLqfYrOnFWdupH-cDtWuf2JLBWN6AAYqVZ)

* [Crossfit 101 - The Basics](https://www.youtube.com/watch?v=eBvy_A2jTmk "Crossfit 101 - The Basics")

* [How to fix your POSTURE in 10 Minutes (Prevent Injury, Realign your Spine, Perform Better) - YouTube](https://youtu.be/urGJm1XwFsQ)

---

# Other Helpful Links

## Timer websites

* [HIIT Clock](https://www.hiitclock.com/)
* [https://www.online-stopwatch.com/interval-timer/](https://www.online-stopwatch.com/interval-timer/)
* [https://fitatmidlife.com/timer/](https://fitatmidlife.com/timer/)
* 

---

# Workouts

## Daily Posture Work at Desk - [How to fix your POSTURE in 10 Minutes (Prevent Injury, Realign your Spine, Perform Better) - YouTube](https://youtu.be/urGJm1XwFsQ)

**2 rounds x 1 minute of each movement (from the video above**)

- Crouching chest stretch

- Chin tuck - bring chin back

- Reverse fly - bend over, pinch traps, bring arms back

- Lunge with side lean - 1 min each side

- Plank with chin tuck

- Plantar stretch

## Daily Morning Workout

### Daily Warmup

* Crossover Symmetry - Rotator cuff exercises 

* 15 jumping jax

* 15 side-to-side jax

* 10 Inchworms

* 10 hip thrusters

* 15 Knee hugs

* 30s primal squat

* Spiderman t spine rotations x 10/side
  
  Front Rack Mobility x 30s/arm
  
  - Banded Glute bridges x 10

### Daily Metcon

* 2 rounds
  * 20 rolling situps
  * 20 push ups
  * 20 toes to wall
  * 5x 30 secs weighted superman
  * 20 dumbell thrusters
  * 20 weighted lunges
  * 2 minutes plank, choice
* Stretching

## Standardized Workout

### Warm up

SMR 5 mins
Then do the following:

* Hip mobilizations x 10/leg
* Rolling cossack x 8/leg
* Leg lowers x 10/leg
* Banded ankle mobility x 30s/ankle
* Spiderman t spine rotations x 10/side
* Front Rack Mobility x 30s/arm
* Primal Squat x 30s
* Banded Glute bridges x 10
* Banded Goodmornings x 12
* 10 meter inch worm
* 10 meter bear crawl
* 10 meter high knees

### Workout - Do 3 rounds focusing on form

* 20 Roll-up situps
* 20 Push ups
* 10 Air squats, with weight\
* 20 Toe touch leg lifts
* 5 Pull ups
* 30 Seconds side plank each side
* 10 Front lunges, with weight
* 10 Dumbell shoulder press
* 40 Seconds front plank
* 10 x 5 Seconds Weighted superman
* 20 cals rowing
* 30 seconds hollow hold

### For Strength

* Bar work: Bear Complex:  1 Power Clean, 1 Front Squat, 1 Push Press, 1 Back Squat, 1 Back Rack Push Press as individual movements

## WED JULY 11, 2018 CHALLENGE CIRCUIT

### Total Body Warm-UP

2 rounds through

* Shoulder taps - 10 taps per side
* Bear Crawl - 10 steps forward
* Bird Dog - 10 reps
* Single Leg- Hip-Thrusts - 10 per leg

#### A: Metcon (AMRAP - Rounds)

* Strength- Cardio Circuit 1- 3 rounds
* Deadlifts-15 reps
* Ring rows- 15
* Bear Crawls 40 meters

#### B: Metcon (AMRAP - Rounds)

* Strength - Cardio 2 -  3 rounds
* Heavy Step-ups - 10 per leg
* Push-ups  - 15
* Heavy Carries - 40 meters

#### C: Stretch

* 5 min stretch and breathe

## JULY 10, 2018 CHALLENGE CIRCUIT

### Total Body Warm-UP July 10

2 rounds through

* 15 knee hugs each side
* 10 inch worms
* 10 twisting planks each side
* 15 walking lunges
* 15 hip thrusters

### Workout

* Heavy Carries - 40 meters

#### C: Stretch July 10

* 5 min stretch and breathe

## July 20, 2018

### Warm up N

* DEADLIFT / SQUAT Warm up**
* SMR 5 mins
* hip mobilizations x 10/leg
* rolling cossack x 8/leg
* leg lowers x 10/leg
* banded ankle mobility x 30s/ankle
* t spine rotations x 10/side
* Front Rack Mobility x 30s/arm
* Primal Squat x 30s
* Banded Glute bridges x 10
* Banded Goodmornings x 12

### Workout July 20

IF SQUATTING:

* Goblet Squats x 10
* Box Jumps x 5

IF DEADLIFTING

* KBS x 6
* Deadlift 2 x 5(FAST @ 50-60%) 1 min between sets

## Rep work

* A1: Safety Squat Bar (4 x 5(3s pause each rep))
  
  * keep weight at a level where you can maintain chest staying upright the entire time; better to   * * go light if need be

* A2: Feet Elevated Ring Rows (4 x 8)

* A3: Side Plank w/ Rotation (4 x 8/rotations per arm) <https://www.youtube.com/watch?v=7XkGSzPq4Y4>

* B1: Single leg lunge hold w/paloff press (3 x 20s/leg) <https://www.youtube.com/watch?v=55bUEAWcgmE>

* B2: Pull Up Hold (3 x 30s)
  
  * -keep shoulder away from ears to put humeral head in proper position

### Metcon July 20

* Metcon (No Measure)
  * 1 min each station 3 round
  * continuous grind(keep intensity at 80%)
  * -Skierg
  * -Single unders
  * -sit ups

## THU JULY 19, 2018 WEEK 2 DAY 4

* DEADLIFT / SQUAT Warm up**
* SMR 5 mins
* hip mobilizations x 10/leg
* rolling cossack x 8/leg
* leg lowers x 10/leg
* banded ankle mobility x 30s/ankle
* spiderman t spine rotations x 10/side
* Front Rack Mobility x 30s/arm
* Primal Squat x 30s
* Banded Glute bridges x 10
* Banded Goodmornings x 12

IF SQUATTING:

* Goblet Squats x 10
* Box Jumps x 5

IF DEADLIFTING

* KBS x 6
* Deadlift 2 x 5(FAST @ 50-60%) 1 min between sets
* A: Deadlift (4 x 8(2s Pause on floor after each rep))
* B1: OH Tricep Extensions (3 x 10)
* B2: Sled Drag (3 x 40m)
* B3: Knees to Chest (3 x 10)

### Metcon

Metcon (No Measure)

* 10 min EMOM
* 12 cal bike
* 1 min plank

## THU JULY 19, 2018 HOME WORKOUT 2

Home workout warm-up
2 rounds, 10 of each as a circuit:

* A1. Air Squats
  * [<https://www.youtube.com/watch?v=C_VtOYc6j5c>]([https://www.youtube.com/watch?v=C_VtOYc6j5c](https://www.youtube.com/watch?v=C_VtOYc6j5c)
* A2. Knee Hugs on the spot
  * [<https://www.youtube.com/watch?v=1WSQSNlu_OQ>]([https://www.youtube.com/watch?v=1WSQSNlu_OQ](https://www.youtube.com/watch?v=1WSQSNlu_OQ)
* A3. Forward Lunges
  * [<https://www.youtube.com/watch?v=VSLz1HB2eWM>]([https://www.youtube.com/watch?v=VSLz1HB2eWM](https://www.youtube.com/watch?v=VSLz1HB2eWM)
* A4. Inchworms
  * [<https://www.youtube.com/watch?v=GakR3jpwbqs>]([https://www.youtube.com/watch?v=GakR3jpwbqs](https://www.youtube.com/watch?v=GakR3jpwbqs)

### Metcon (Time)

Only rest  if you need to, otherwise continue until you complete all 4 and then rest 60 sec.

* B1. Mountain Climbers- 60 seconds
  
  * <https://www.youtube.com/watch?v=fBZHkGT0W5Y>

* B2. Plank - 60 seconds
  
  * <https://www.youtube.com/watch?v=KB-NoOo4mac>

* B3. Mountain Climbers- 60 sec

* B4. Plank - 60 sec

* Rest 60 seconds and repeat again.

* Metcon (Time)

* Complete 4 rounds through.

* C1. Air Squats- 60 seconds
  
  * <https://www.youtube.com/watch?v=C_VtOYc6j5c>

* C2. Inchworm with push-up(if push-up is possible)- 60 sec
  
  * <https://www.youtube.com/watch?v=GakR3jpwbqs><>

* C3. Forward Lunges- 60 sec
  
  * [<https://www.youtube.com/watch?v=VSLz1HB2eWM>]([https://www.youtube.com/watch?v=VSLz1HB2eWM](https://www.youtube.com/watch?v=VSLz1HB2eWM))

* C4. Plank into Push-up -60 sec (switch pushing arm at 30 sec mark)
  
  * <https://www.youtube.com/watch?v=yy6VSYBqrtA>

## TUE JULY 17, 2018 HOME WORKOUT 1

Home workout warm-up
2 rounds, 10 of each as a circuit:

* A1. Air Squats
  * <https://www.youtube.com/watch?v=C_VtOYc6j5c>
* A2. Knee Hugs on the spot
  * <https://www.youtube.com/watch?v=1WSQSNlu_OQ>
* A3. Forward Lunges
  * <https://www.youtube.com/watch?v=VSLz1HB2eWM>
* A4. Inchworms
  * <https://www.youtube.com/watch?v=GakR3jpwbqs>

Metcon (Time)

* 4 rounds through with 60 seconds rest in between rounds (at the end of B4)

* B1. High Knees - 60 seconds
  
  * <https://www.youtube.com/watch?v=KUgaQBg2RyY>

* B2. Feet In-and-Out- 60 seconds (stay on the ball of your feet, and you don't need a platform for this, pretend you have one)
  
  * <https://www.youtube.com/watch?v=Gdxu87EjS6U>

* B3. Touch down jump-squat- 60 seconds
  
  * <https://www.youtube.com/watch?v=_4d8ludOHzs>

* B4. Burpees - 60 seconds (pick your progression out of those)
  
  * <https://www.youtube.com/watch?v=8oWDiz_E-yI>

### Metcon -

Metcon (AMRAP - Rounds)

* AMRAP 10 mins
* 200m Run / Row
* 15 Air Squats
* 45s Hollow Hold

## MON JULY 30, 2018 WEEK 4 DAY 1

BENCH Warm up** SMR 5 mins

* hip mobilizations x 10
* snow angels on foam roller  x 10
* bench t spine mobilizations x 10
* banded ankle mobility x 30s/ankle
* banded glute bridges x 10
* lat pull downs with band x 10
* external rotations x 8/arm
* Speed Push Ups x 5(do clapping push ups if able)

### Workout Adult class

* A1: Close Grip Bench Press (5 x 6(31x1))

* A2: DB Superman's (4 x 8(do w/o weight if need to))
  
  * <https://www.youtube.com/watch?v=A6RXOJ3FxUA>

* 60s Rest

* B1: Trap Bar Deadlift (3 x 8)

* B2: 1/2 Kneeling Landmine Press (3 x 6/arm) - push barbell up and forward with one arm on one knee
  
  * <https://www.youtube.com/watch?v=_ArzG9qz-yM>

### 12 min EMOM

Metcon (No Measure)

* Min #1: Med ball sit up w/OH Throw x 30s
* Min #2: Single unders x 60 - Jummp rope or toe jumps
* Min #3: KBS x 10 - kettle bell swings under the groin
* Min #4: 10 MF's - Burpees with dumbell curl to shoulder press.

## TUE JULY 31, 2018 WEEK 4 DAY 2

### DEADLIFT / SQUAT Warm up**

* SMR 5 mins
* hip mobilizations x 10/leg
* rolling cossack x 8/leg
* leg lowers x 10/leg
* banded ankle mobility x 30s/ankle
* spiderman t spine rotations x 10/side
* Front Rack Mobility x 30s/arm
* Primal Squat x 30s
* Banded Glute bridges x 10
* Banded Goodmornings x 12

IF SQUATTING:

* Goblet Squats x 10
* Box Jumps x 5

IF DEADLIFTING

* KBS x 6

* Deadlift 2 x 5(FAST @ 50-60%) 1 min between sets

* A1: Box Squat (Loaded) (5 x 6(31x1))
  
  * <https://www.youtube.com/watch?v=mjm3W3tQejM>

* A2: T Bar Rows (4 x 8)
  
  * Use the T-Bar Attatchment and the landmine attatchment to set up.
  * These rows are done in a deadlift stance over the bar. Slightly upright torso. Focus on the squeeze and a slow lower.

* 60s Rest

* B1: Front Foot Elevated Reverse Lunge (3 x 8/leg(no alternating; elevate foot to comfort))

* B2: Rack Pull Ups (3 x 6-8(do regular pull ups if cannot do this))
  
  * <https://www.youtube.com/watch?v=C7qIgpv_iQs>

### Metcon - Metcon (AMRAP - Rounds)

#### AMRAP 10 mins PARTNER

1. Trap Bar Carry 40m
2. Air Squats x 5 / sit ups x 5 AMRAP

## WED AUGUST 01, 2018 WEEK 4 DAY 3

### cardio Warm Up

* SMR 5-10 mins
* Bike or Row 3 mins
* 1 Round

Down and Back

* -high knees
* -side shuffle
* -bear crawl
* -inchworm

Metcon (Time)

* 10 RFT
* 5 Burpees
* 10 Cal Bike
* 10 Jumping Jacks
* 10 Cal Row
* 5 Burpees
* HR @ 85-88%

## THU AUGUST 02, 2018 WEEK 4 DAY 4

### DEADLIFT / SQUAT Warm up B**

* SMR 5 mins
* hip mobilizations x 10/leg
* rolling cossack x 8/leg
* leg lowers x 10/leg
* banded ankle mobility x 30s/ankle
* spiderman t spine rotations x 10/side
* Front Rack Mobility x 30s/arm
* Primal Squat x 30s
* Banded Glute bridges x 10
* Banded Goodmornings x 12

IF SQUATTING:

* Goblet Squats x 10
* Box Jumps x 5

IF DEADLIFTING

* KBS x 6

* Deadlift 2 x 5(FAST @ 50-60%) 1 min between sets

* A: Deadlift (4 x 6(2s Pause on floor after each rep))

* B1: OH Tricep Extensions (3 x 12)

* B2: Sled Drag (3 x 40m)

* B3: Knees to Chest (3 x 10)

Metcon

* 10 min EMOM
* -TGU 2/side
* -1 min plank

## FRI AUGUST 03, 2018 WEEK 4 DAY 5

### DEADLIFT / SQUAT Warm up 2 --

* SMR 5 mins
* hip mobilizations x 10/leg
* rolling cossack x 8/leg
* leg lowers x 10/leg
* banded ankle mobility x 30s/ankle
* spiderman t spine rotations x 10/side
* Front Rack Mobility x 30s/arm
* Primal Squat x 30s
* Banded Glute bridges x 10
* Banded Goodmornings x 12

IF SQUATTING:

* Goblet Squats x 10
* Box Jumps x 5

IF DEADLIFTING

* KBS x 6
* Deadlift 2 x 5(FAST @ 50-60%) 1 min between sets
* A1: Safety Squat Bar (4 x 5(5s pause first rep only))
  * keep weight at a level where you can maintain chest staying upright the entire time; better to go light if need be
* A2: Feet Elevated Ring Rows (4 x 8)
* A3: Side Plank w/ Rotation (4 x 8/rotations per arm) <https://www.youtube.com/watch?v=7XkGSzPq4Y4>
* B1: Single leg lunge hold w/paloff press (3 x 25s/leg) <https://www.youtube.com/watch?v=55bUEAWcgmE>
* B2: Pull Up Hold (3 x 30s)
  * -keep shoulder away from ears to put humeral head in proper position

### Metcon --

* Row 80 cals

## THU AUGUST 02, 2018 HOME WORKOUT 2

### Home workout warm-up

* 2 rounds, 10 of each as a circuit:

* A1. Air Squats
  
  * <https://www.youtube.com/watch?v=C_VtOYc6j5c>

* A2. Knee Hugs on the spot
  
  * <https://www.youtube.com/watch?v=1WSQSNlu_OQ>

* A3. Forward Lunges
  
  * <https://www.youtube.com/watch?v=VSLz1HB2eWM>

* A4. Inchworms
  
  * <https://www.youtube.com/watch?v=GakR3jpwbqs>

### Metcon (Time) --

Only rest  if you need to, otherwise continue until you complete all 4 and then rest 60 sec.

* B1. Mountain Climbers- 60 seconds
  * <https://www.youtube.com/watch?v=fBZHkGT0W5Y>
* B2. Plank - 60 seconds
  * <https://www.youtube.com/watch?v=KB-NoOo4mac>
* B3. Mountain Climbers- 60 sec
* B4. Plank - 60 sec

Rest 60 seconds and repeat again.

### Metcon (Time) ++

Complete 4 rounds through.

* C1. Air Squats- 60 seconds
  * <https://www.youtube.com/watch?v=C_VtOYc6j5c>
* C2. Inchworm with push-up(if push-up is possible)- 60 sec
  * <https://www.youtube.com/watch?v=GakR3jpwbqs>
* C3. Forward Lunges- 60 sec
  * <https://www.youtube.com/watch?v=VSLz1HB2eWM>
* C4. Plank into Push-up -60 sec (switch pushing arm at 30 sec mark)
  * <https://www.youtube.com/watch?v=yy6VSYBqrtA>

Rest 60 sec and repeat 3 more times

## TUE AUGUST 07, 2018 WEEK 5 DAY 2

DEADLIFT / SQUAT Warm up**

* SMR 5 mins
* hip mobilizations x 10/leg
* rolling cossack x 8/leg
* leg lowers x 10/leg
* banded ankle mobility x 30s/ankle
* spiderman t spine rotations x 10/side
* Front Rack Mobility x 30s/arm
* Primal Squat x 30s
* Banded Glute bridges x 10
* Banded Goodmornings x 12

IF SQUATTING:

* Goblet Squats x 10
* Box Jumps x 5
* IF DEADLIFTING
* KBS x 6

Deadlift 2 x 5(FAST @ 50-60%) 1 min between sets

* A1: Box Squat (Loaded) (5 x 4(21x1))
  * <https://www.youtube.com/watch?v=mjm3W3tQejM>
* A2: T Bar Rows (4 x 8)

Use the T-Bar Attatchment and the landmine attatchment to set up.
These rows are done in a deadlift stance over the bar. Slightly upright torso. Focus on the squeeze and a slow lower.

* 60s Rest
* B1: Front Foot Elevated Reverse Lunge (3 x 8/leg(no alternating; elevate foot to comfort))
* B2: Rack Pull Ups (3 x 6-8)
  * <https://www.youtube.com/watch?v=C7qIgpv_iQs>

Progressions:

* 1.rack pull up negatives for 6-8 reps
* 2.rack pull up HOLD for 20 seconds
* 3.rack pull up
* 4.weighted rack pull up with chains around neck(only paul does this)

### Metcon 2

Metcon (AMRAP - Rounds)
AMRAP 10 mins PARTNER

* 1 Trap Bar Carry 40m
* 2 Air Squats x 5 / sit ups x 5 AMRAP

## TUE AUGUST 07, 2018 HOME WORKOUT 1

Home workout warm-up
2 rounds, 10 of each as a circuit:

* A1. Air Squats
  * <https://www.youtube.com/watch?v=C_VtOYc6j5c>
* A2. Knee Hugs on the spot
  * <https://www.youtube.com/watch?v=1WSQSNlu_OQ>
* A3. Forward Lunges
  * <https://www.youtube.com/watch?v=VSLz1HB2eWM>
* A4. Inchworms
  * <https://www.youtube.com/watch?v=GakR3jpwbqs>

### Metcon (Time) 2

 4 rounds through with 60 seconds rest in between rounds (at the end of B4)

* B1. High Knees - 60 seconds
  * <https://www.youtube.com/watch?v=KUgaQBg2RyY>
* B2. Feet In-and-Out- 60 seconds (stay on the ball of your feet, and you don't need a platform for this, pretend you have one)   * <https://www.youtube.com/watch?v=Gdxu87EjS6U>
* B3. Touch down jump-squat- 60 seconds
  * <https://www.youtube.com/watch?v=_4d8ludOHzs>
* B4. Burpees - 60 seconds (pick your progression out of those)
  * <https://www.youtube.com/watch?v=8oWDiz_E-yI>

## THU AUGUST 09, 2018 HOME WORKOUT 2

Home workout warm-up
2 rounds, 10 of each as a circuit:

* A1. Air Squats
  * <https://www.youtube.com/watch?v=C_VtOYc6j5c>
* A2. Knee Hugs on the spot
  * <https://www.youtube.com/watch?v=1WSQSNlu_OQ>
* A3. Forward Lunges
  * <https://www.youtube.com/watch?v=VSLz1HB2eWM>
* A4. Inchworms
  * <https://www.youtube.com/watch?v=GakR3jpwbqs>

### Metcon (Time) 4

Only rest  if you need to, otherwise continue until you complete all 4 and then rest 60 sec.

* B1. Mountain Climbers- 60 seconds
  * <https://www.youtube.com/watch?v=fBZHkGT0W5Y>
* B2. Plank - 60 seconds
  * <https://www.youtube.com/watch?v=KB-NoOo4mac>
* B3. Mountain Climbers- 60 sec
* B4. Plank - 60 sec

Rest 60 seconds and repeat again.

### Metcon (Time) 5

Complete 4 rounds through.

* C1. Air Squats- 60 seconds
  * <https://www.youtube.com/watch?v=C_VtOYc6j5c>
* C2. Inchworm with push-up(if push-up is possible)- 60 sec
  * <https://www.youtube.com/watch?v=GakR3jpwbqs>
* C3. Forward Lunges- 60 sec
  * <https://www.youtube.com/watch?v=VSLz1HB2eWM>
* C4. Plank into Push-up -60 sec (switch pushing arm at 30 sec mark)
  * <https://www.youtube.com/watch?v=yy6VSYBqrtA>

Rest 60 sec and repeat 3 more times

## WED AUGUST 08, 2018 WEEK 5 DAY 3

### cardio Warm Up B

* SMR 5-10 mins
* Bike or Row 3 mins
* 1 Round
  * Down and Back
  * -high knees
  * -side shuffle
  * -bear crawl
  * -inchworm

### Metcon (Time) 7

10 RFT

* 5 Box Jumps
* 10 T2P(Toes to Post)
* 20 Cal Row
* 5 Burpees
* HR @ 85-88%

## WED AUGUST 01, 2018 WEEK 4 DAY 3b

### cardio Warm Up C

* SMR 5-10 mins
* Bike or Row 3 mins
* 1 Round
* Down and Back
* -high knees
* -side shuffle
* -bear crawl
* -inchworm

### Metcon (Time) 8

10 RFT

* 5 Burpees
* 10 Cal Bike
* 10 Jumping Jacks
* 10 Cal Row
* 5 Burpees
* HR @ 85-88%

## MON AUGUST 06, 2018 WEEK 5 DAY 1

Warm up
SMR 5 mins

* hip mobilizations x 10
* snow angels on foam roller  x 10
* bench t spine mobilizations x 10
* banded ankle mobility x 30s/ankle
* banded glute bridges x 10
* lat pull downs with band x 10
* external rotations x 8/arm
* Speed Push Ups x 5(do clapping push ups if able)

### Strength 6-Aug-18

* A1: Close Grip Bench Press (5 x 4(21x1))
* A2: DB Superman's (4 x 8(do w/o weight if need to))
  * <https://www.youtube.com/watch?v=A6RXOJ3FxUA>
* 60s Rest
* B1: Trap Bar Deadlift (3 x 8)
* B2: 1/2 Kneeling Landmine Press (3 x 6/arm)
  * <https://www.youtube.com/watch?v=_ArzG9qz-yM>

### Metcon (No Measure) EMOM 9 Mins

* Min #1: Bike 10 Cals
* Min #2: KBS x 15
* Min #3: 10 MF's

## MON AUGUST 13, 2018 WEEK 6 DAY 1

Warm up
SMR 5 mins

* hip mobilizations x 10
* snow angels on foam roller  x 10
* bench t spine mobilizations x 10
* banded ankle mobility x 30s/ankle
* banded glute bridges x 10
* lat pull downs with band x 10
* external rotations x 8/arm
* Speed Push Ups x 5(do clapping push ups if able)

### Strength 13-Aug-18

* A: Close Grip Bench Press (5 x 4(21x1))

* B: Trap Bar Deadlift (3 x 8)

* 12 min EMOM
  
  * 1: DB Shoulder Press (3 x 10)
  * 2: Barbell RDL (3 x 10)
  * 3: AirDyne Sprint 30s (Calories)
  * As many calories as you can

* 4: Tuck Hold (parallette)  (3 x 40s)
  
  * <https://www.youtube.com/watch?v=HCtSnyng0us>
  1) Start in a seated tucked
     position
  2) Knees toward chest
  3) Push arms straight down
  4) Maintain balance

## TUE AUGUST 14, 2018 WEEK 6 DAY 2

### Warmup 14-aug-18

SMR 5 mins

* hip mobilizations x 10/leg
* rolling cossack x 8/leg
* leg lowers x 10/leg
* banded ankle mobility x 30s/ankle
* spiderman t spine rotations x 10/side
* Front Rack Mobility x 30s/arm
* Primal Squat x 30s
* Banded Glute bridges x 10
* Banded Goodmornings x 12

IF SQUATTING:

* Goblet Squats x 10
* Box Jumps x 5

IF DEADLIFTING

* KBS x 6

Deadlift 2 x 5(FAST @ 50-60%) 1 min between sets

* A1: Box Squat (Loaded) (5 x 4(21x1))
* [<https://www.youtube.com/watch?v=mjm3W3tQejM>]([https://www.youtube.com/watch?v=mjm3W3tQejM](https://www.youtube.com/watch?v=mjm3W3tQejM))
* A2: Ring Pull-Ups (4 x 6-8)
* A2: Pull-Ups (4 x 6-8(Do this if you can't perform ring pull up))
* A3: Controlled Leg Lowering (4 x 10)

This exercise focuses on lumbar control through a dynamic movement. If an FMS trunk stability score is scored as a 1, this is a great exercise in order to teach motor control of the lumbo pelvic region.

### Metcon 14-aug-18

* 20 min AMRAP- Rounds
  * -front elevated reverse lunges w/DB in front rack position x 5/leg
  * -barbell inverted rows x 8
  * -Roll Ups x 10
  * -row 500m
  * Keep HR out of Red Zone

## WED AUGUST 15, 2018 WEEK 6 DAY 3

### Cardio Warm Up

SMR 5-10 mins

* Bike or Row 3 mins
* 1 Round Down and Back
  * -high knees
  * -side shuffle
  * -bear crawl
  * -inchworm

### Metcon (AMRAP - Rounds) 15-aug-18

* 35 min AMRAP
  * -5 wall balls
  * -20 cal row
  * -20 jumping jacks
  * -10 KB DL
  * HR @ 85-88%

## THU AUGUST 16, 2018 WEEK 6 DAY 4

SMR 5 mins

* hip mobilizations x 10/leg
* rolling cossack x 8/leg
* leg lowers x 10/leg
* banded ankle mobility x 30s/ankle
* spiderman t spine rotations x 10/side
* Front Rack Mobility x 30s/arm
* Primal Squat x 30s
* Banded Glute bridges x 10
* Banded Goodmornings x 12

IF SQUATTING:

* Goblet Squats x 10
* Box Jumps x 5

IF DEADLIFTING

* KBS x 6

* Deadlift 2 x 5(FAST @ 50-60%) 1 min between sets
  
  * A: Deadlift (4 x 4(2s pause on floor after each rep))
  * B: Floor Press (4 x 8(60s between sets))

### Metcon (No Measure)

* 5 RFT
  * -12 burpees
  * -30m KB OH Carry
  * -12 KBS

## FRI AUGUST 17, 2018 WEEK 6 DAY 5

### DEADLIFT / SQUAT Warm up 8/17**

SMR 5 mins

* hip mobilizations x 10/leg
* rolling cossack x 8/leg
* leg lowers x 10/leg
* banded ankle mobility x 30s/ankle
* spiderman t spine rotations x 10/side
* Front Rack Mobility x 30s/arm
* Primal Squat x 30s
* Banded Glute bridges x 10
* Banded Goodmornings x 12

IF SQUATTING:

* Goblet Squats x 10
* Box Jumps x 5

IF DEADLIFTING

* KBS x 6

* Deadlift 2 x 5(FAST @ 50-60%) 1 min between sets

* A1: Safety Squat Bar off Box (4 x 8(8 RPE))
  
  * <https://www.youtube.com/watch?v=2bkidbmb8Tk>

* A2: Single Arm Row (4 x 8)
  
  * <https://www.youtube.com/watch?v=pYcpY20QaE8>

### Metcon 8/17

6 rounds

* 1: Barbell Bridge (10 reps)
* 2: KB Alternating Pull Through (12 reps)
* 2: Run 200m (200m)

## TUE AUGUST 14, 2018 HOME WORKOUT 1

### Home workout warm-up 8/14

2 rounds, 10 of each as a circuit:

* A1. Air Squats
  * <https://www.youtube.com/watch?v=C_VtOYc6j5c>
* A2. Knee Hugs on the spot
  * <https://www.youtube.com/watch?v=1WSQSNlu_OQ>
* A3. Forward Lunges
  * <https://www.youtube.com/watch?v=VSLz1HB2eWM>
* A4. Inchworms
  * <https://www.youtube.com/watch?v=GakR3jpwbqs>

### Metcon (Time) 8/14

4 rounds through with 60 seconds rest in between rounds (at the end of B4)

* B1. High Knees - 60 seconds
  * <https://www.youtube.com/watch?v=KUgaQBg2RyY>
* B2. Feet In-and-Out- 60 seconds (stay on the ball of your feet, and you don't need a platform for this, pretend you have one) <https://www.youtube.com/watch?v=Gdxu87EjS6U>
* B3. Touch down jump-squat- 60 seconds
  * <https://www.youtube.com/watch?v=_4d8ludOHzs>
* B4. Burpees - 60 seconds (pick your progression out of those)
  * <https://www.youtube.com/watch?v=8oWDiz_E-yI>

## THU AUGUST 16, 2018 HOME WORKOUT 2

### Home workout warm-up 8/16

2 rounds, 10 of each as a circuit:

* A1. Air Squats
  * <https://www.youtube.com/watch?v=C_VtOYc6j5c>
* A2. Knee Hugs on the spot
  * <https://www.youtube.com/watch?v=1WSQSNlu_OQ>
* A3. Forward Lunges
  * <https://www.youtube.com/watch?v=VSLz1HB2eWM>
* A4. Inchworms
  * <https://www.youtube.com/watch?v=GakR3jpwbqs>

### Metcon (Time) 8/16

Only rest  if you need to, otherwise continue until you complete all 4 and then rest 60 sec.

* B1. Mountain Climbers- 60 seconds
  * <https://www.youtube.com/watch?v=fBZHkGT0W5Y>
* B2. Plank - 60 seconds
  * <https://www.youtube.com/watch?v=KB-NoOo4mac>
* B3. Mountain Climbers- 60 sec
* B4. Plank - 60 sec

Rest 60 seconds and repeat again.

### Metcon (Time) B

Complete 4 rounds through.

* C1. Air Squats- 60 seconds
  * <https://www.youtube.com/watch?v=C_VtOYc6j5c>
    C2. Inchworm with push-up(if push-up is possible)- 60 sec
    <https://www.youtube.com/watch?v=GakR3jpwbqs>
    C3. Forward Lunges- 60 sec
    <https://www.youtube.com/watch?v=VSLz1HB2eWM>
    C4. Plank into Push-up -60 sec (switch pushing arm at 30 sec mark)
    <https://www.youtube.com/watch?v=yy6VSYBqrtA>

Rest 60 sec and repeat 3 more times

## MON AUGUST 06, 2018 CHALLENGE 10

* * [ ] 5 minutes Rolling
  
  * [ ] 5 minutes Rower or Bike

1 round of:

- Walking Knee Hugs

- Inchworms

- Groin Stretch

- T-Plank Rotations

3 sets

A1. Goblet Box Squat- 10 reps
A2. Overhead Dumbbell Press - 10 reps

B1. Forward Lunge- 10 reps
B2. Push-ups - AMRAP

Medball Slams - Tabata

Stretch and chat

## MON AUGUST 27, 2018 PREP WEEK DAY 1

BENCH Warm up**
SMR 5 mins
hip mobilizations x 10
snow angels on foam roller  x 10
bench t spine mobilizations x 10
banded ankle mobility x 30s/ankle
banded glute bridges x 10
lat pull downs with band x 10
external rotations x 8/arm
Speed Push Ups x 5(do clapping push ups if able)
A: Bench Press (4 x 5/5/3/1(7 RPE))
B: Trap Bar Deadlift (3 x 8(dont go heavy))
12 min EMOM
1: DB Shoulder Press (3 x 10)
2: Barbell RDL (3 x 10)
3: AirDyne Sprint 30s (Calories)
As many calories as you can
4: Tuck Hold (parallette)  (3 x 40s)
<https://www.youtube.com/watch?v=HCtSnyng0us>

1) Start in a seated tucked
   position
2) Knees toward chest
3) Push arms straight down
4) Maintain balance

## TUE AUGUST 28, 2018 PREP WEEK DAY 2

DEADLIFT / SQUAT Warm up**
SMR 5 mins
hip mobilizations x 10/leg
rolling cossack x 8/leg
leg lowers x 10/leg
banded ankle mobility x 30s/ankle
spiderman t spine rotations x 10/side
Front Rack Mobility x 30s/arm
Primal Squat x 30s
Banded Glute bridges x 10
Banded Goodmornings x 12
IF SQUATTING:
Goblet Squats x 10
Box Jumps x 5
IF DEADLIFTING
KBS x 6
Deadlift 2 x 5(FAST @ 50-60%) 1 min between sets
A1: Back Squat (4 x 5/5/3/1(7 RPE))
A2: Ring Pull-Ups (4 x 6-8)
A2: Pull-Ups (4 x 6-8(Do this if you can't perform ring pull up))
A3: Controlled Leg Lowering (4 x 10)
This exercise focuses on lumbar control through a dynamic movement. If an FMS trunk stability score is scored as a 1, this is a great exercise in order to teach motor control of the lumbo pelvic region.
Metcon
Metcon (AMRAP - Rounds)
20 min AMRAP
-front elevated reverse lunges w/DB in front rack position x 5/leg
-barbell inverted rows x 8
-Roll Ups x 10
-row 500m
Keep HR out of Red Zone

## WED AUGUST 29, 2018 PREP WEEK DAY 3

**cardio Warm Up
SMR 5-10 mins
Bike or Row 3 mins
1 Round
Down and Back
-high knees
-side shuffle
-bear crawl
-inchworm
Metcon (No Measure)
35 min AMRAP
-Farmers Carry x 40m
-Wall Balls x 10
-Row 500m
-Standing Inchworms x 5
-Hollow Hold x 30s
Keep HR @ 85-88%

## THU AUGUST 30, 2018 PREP WEEK DAY 4

DEADLIFT / SQUAT Warm up**
SMR 5 mins
hip mobilizations x 10/leg
rolling cossack x 8/leg
leg lowers x 10/leg
banded ankle mobility x 30s/ankle
spiderman t spine rotations x 10/side
Front Rack Mobility x 30s/arm
Primal Squat x 30s
Banded Glute bridges x 10
Banded Goodmornings x 12
IF SQUATTING:
Goblet Squats x 10
Box Jumps x 5
IF DEADLIFTING
KBS x 6
Deadlift 2 x 5(FAST @ 50-60%) 1 min between sets
A: Deadlift (4 x 5/5/3/1(7 RPE))
B: Floor Press (4 x 6(7 RPE)60s between sets)
Metcon
Metcon (No Measure)
3 RFT
-12 burpees
-30m KB OH Carry
-12 KBS

## FRI AUGUST 31, 2018 PREP WEEK DAY 5

DEADLIFT / SQUAT Warm up**
SMR 5 mins
hip mobilizations x 10/leg
rolling cossack x 8/leg
leg lowers x 10/leg
banded ankle mobility x 30s/ankle
spiderman t spine rotations x 10/side
Front Rack Mobility x 30s/arm
Primal Squat x 30s
Banded Glute bridges x 10
Banded Goodmornings x 12
IF SQUATTING:
Goblet Squats x 10
Box Jumps x 5
IF DEADLIFTING
KBS x 6
Deadlift 2 x 5(FAST @ 50-60%) 1 min between sets
A1: Safety Squat Bar off Box (3 x 5(6 RPE))
<https://www.youtube.com/watch?v=2bkidbmb8Tk>
A2: Single Arm Row (3 x 8)
<https://www.youtube.com/watch?v=pYcpY20QaE8>
Metcon
6 rounds
1: Barbell Bridge (10 reps)
2: KB Alternating Pull Through (12 reps)
3.: Airdyne (1:00(easy pace))

## SAT SEPTEMBER 01, 2018 PREP WEEK DAY 6

**cardio Warm Up
SMR 5-10 mins
Bike or Row 3 mins
1 Round
Down and Back
-high knees
-side shuffle
-bear crawl
-inchworm
Metcon (No Measure)
30 mins RECOVERY AMRAP
-20 cals bike(easy pace)
-8 Rolling Cossacks/leg
-20 cal row(easy pace)
-10 hip mobilizations/leg
-10 RDLs(bar only)

## Good Workout

**cardio Warm Up
SMR 5-10 mins
Bike or Row 3 mins
1 Round
Down and Back
-high knees
-side shuffle
-bear crawl
-inchworm

"Cindy"
Complete as many rounds in 20 minutes as you can of:
5 Pull-ups
10 Push-ups
15 Squats

OR

"Mary"
Complete as many rounds in 20 minutes as you can of:
5 Handstand Push-ups
10 One legged squats, alternating
15 Pull-ups

## MON SEPTEMBER 03, 2018 TEST DAY 1

*Back Squat Warm-Up*
Foam Roll 3-5min

10 half kneeling hip mobilizations each side
10 prayer stretches
10 t-spine rotations on foam roller
30s/arm Front Rack Mobility Stretch

10 leg lowers on rig/leg
20 Scorpions
20 Alt arm /legs Bird Dog

1 Round
20m Lateral Walk
20 Bridges NO Weight
20 Banded Good mornings
A: Back Squat (1 RM(20x0))
start with empty bar for 10 reps
1st set:60% x 6
2nd set:70% x 3
3rd set:80% x 2
4th set 90% x 1
5th set 100% x 1
6th set 100%+ x 1
rest 2 mins in between sets for first 4 sets; then every set after that rest 5 mins or more depending on how you feel
B: Plank Hold (Right Side)
C: Plank Hold (Left Side)

## TUE SEPTEMBER 04, 2018 TEST DAY 2

* Shoulder Day

SMR: 3-5 minutes

15 quadruped t-spine rotations each side

20 Pass Throughs / 10 ATW with bands
20 Pinch & Press(from pike position)
8 Ext Rotations
8 Full Can
8 Empty Can

2 Rounds
10 Bottoms Up press(5 per side)
8 Halos
30s Hollow hold
A: Bench Press (1 RM(20x0))
start with empty bar for 10 reps
1st set:60% x 6
2nd set:70% x 3
3rd set:80% x 2
4th set 90% x 1
5th set 100% x 1
6th set 100%+ x 1
rest 2 mins in between sets for first 4 sets; then every set after that rest 5 mins or more depending on how you feel
2k Row (Time)
Max Effort 2k Row

## WED SEPTEMBER 05, 2018 TEST DAY 3

* Sweet Aero Warm-Up *

Warmup:
Row/Bike/Run 5 -10 Min - You Choose. Get Blood Moving
Wrist Circles 10-15 each direction
Elbow Circles 10-15 each direction
Arm Circles 10-15 each direction
Arm Rotations 10-15 each direction
Tricep Stretch Side Bends 10-15 each direction
Torso Rotations 10-15 each direction
Hip Circles 10-15 each direction
Walking Lunge and Twist 10-15 each leg
Side Lunge 10-15 each leg

Row or AD Below

2 Rounds
10 sec HARD
Walk 90 sec
Metcon (No Measure)
Recovery AMRAP 40 mins
-20 cal Row
-30s Childs Pose
-20 cal Bike
-30s Cobra Pose
-10 Downward Dogs
Green and Blue Zone ONLY(60-79% Max HR)

## THU SEPTEMBER 06, 2018 TEST DAY 4

*Deadlift Warm-Up*
Row 300m

10 toe elevated toe touches
10 heel elevated toe touches
10 half kneeling hip mobilization each side

10 ring rows
20 bridges
20 clam shells
20m lateral walk
20m monster walk
10 x Dorian Deadlifts(bar only)
A: Deadlift (1 RM)
1st set:60% x 6
2nd set:70% x 3
3rd set:80% x 2
4th set 90% x 1
5th set 100% x 1
6th set 100%+ x 1
rest 2 mins in between sets for first 4 sets; then every set after that rest 5 mins or more depending on how you feel

## FRI SEPTEMBER 07, 2018 TEST DAY 5

* Sweet Aero Warm-Up *

Warmup:
Row/Bike/Run 5 -10 Min - You Choose. Get Blood Moving
Wrist Circles 10-15 each direction
Elbow Circles 10-15 each direction
Arm Circles 10-15 each direction
Arm Rotations 10-15 each direction
Tricep Stretch Side Bends 10-15 each direction
Torso Rotations 10-15 each direction
Hip Circles 10-15 each direction
Walking Lunge and Twist 10-15 each leg
Side Lunge 10-15 each leg

Row or AD Below

2 Rounds
10 sec HARD
Walk 90 sec
A: Burpee Repeat Test (4 Rounds for reps)
40 seconds of max repetitions of burpees, 20 seconds of rest

repeat 4 times
B: Sorensen hold (Max Time)
Arms Across Chest
Press Heels into Metal
Squeeze Butt as Tight as You Can
Hold at Parallel as Long as Possible

## THU SEPTEMBER 13, 2018 WEEK 1 DAY 4

Deadlift Day Warm Up
5 mins SMR
5 min AMRAP
-8 KB DL
-5 Push Ups
-20s Plank
-20s Cobra Pose
5 min Skill Work: Barbell Deadlift (No Measure)
5 reps with 15s on bar focusing on cues  alternating with partner
Set up:
FEET
-put in a position that you feel is strongest; ideally a position that you can initiate the strongest pull with your legs; if this is the first time start feet hip width apart and test it out
HANDS
-placement of the hands should be outside of the knees enough for the arms to have a clear path for the pull without the knees getting in the way
BAR
-should be gently touching the shins(will allow you to use legs more)
BACK
-should be tight creating an arch in your lower back; tighten the lats
When performing the deadlift:
Steps:(assuming you have proper position)
1.tighten glutes and back(like described in setup
2.as you pull keep the bar close to your legs
3.when the bar reaches your knees push your hips forward
4.after lockout push your hips back keeping the bar close to the legs as you lower the bar to the floor

**if having trouble with set up then elevate the bar putting one 45lb plate down on each side of the barbell
A: Deadlift (4 x 10*7 RPE 2 min rest after each set)
2s pause @ knees on way down and 2s pause on floor after rep is completed
B: Close-Grip Floor Press (4 x 6(3s pause on floor after each rep))
1 min rest after each set
Metcon
Metcon (AMRAP - Reps)
8 min AMRAP
Push ups x max reps
every time you fail you have to do 12 med ball slams
***count reps
stretching 5-10 mins (No Measure)
hip external rotation stretch 1 min each leg
ankle mobility x 30s/leg
primal squat 30s
finish by rolling out/stretch tight areas***

## WED SEPTEMBER 12, 2018 WEEK 1 DAY 3

* Sweet Aero Warm-Up *

Warmup:
Row/Bike/Run 5 -10 Min - You Choose. Get Blood Moving
Wrist Circles 10-15 each direction
Elbow Circles 10-15 each direction
Arm Circles 10-15 each direction
Arm Rotations 10-15 each direction
Tricep Stretch Side Bends 10-15 each direction
Torso Rotations 10-15 each direction
Hip Circles 10-15 each direction
Walking Lunge and Twist 10-15 each leg
Side Lunge 10-15 each leg

Row or AD Below

2 Rounds
10 sec HARD
Walk 90 sec
Metcon (No Measure)
30 min AMRAP
10 KBS*do pull throughs for scaling option
300m Row
20m DBL KB Front Rack Carry
10 T2P
HR @ 85-88%

## TUE SEPTEMBER 11, 2018 WEEK 1 DAY 2

Squat Day Warm up
5 mins SMR
3 Rounds
8 squats w/ heartbeat
8 ring rows
20s plank
5 hip circles in each direction on each leg
5 min Skill Work: Back Squat (No Measure)
5 reps EMPTY bar focusing on cues  alternating with partner
Set up:
FEET
-a little bit wider then hip width apart with toes slightly pointed out
HANDS
-placement of the hands should be a little bit wider than shoulder width apart
BAR POSITION
-bar should be on trapezius muscle(big muscle behind the neck)
When performing the squat:
Steps:(assuming you have proper position)
1.walk under the rack place the bar in proper position(as described in the set up) with pelvis just behind the bar
2.before unracking the bar bring elbows just behind the bar pulling the bar gently down enough to activate the muscles around T-Spine(around the shoulder blades)
3.take a couple steps back after unracking the bar and put feet position as described in set up)
4.take a big breath in the stomach and descend into the squat pushing the hips back to initiate going down to parallel
5.after reaching parallel push the knees out as you come back up from the squat
A: Back Squat (4 x 10(31x1)8 RPE*2 min rest between sets)
B1: Pull Up Hold to Eccentric (3 x 4(05x3)*use weight if too easy)
B2: Side Plank Banded Row (3 x 30s/side)
Set up a band on the rig. Tie a band around it. Position yourself in a side plank so that you're facing the rig. Maintain a side plank and pull the band back as if you're driving your elbows behind you to pinch your shoulder blades.

<https://www.youtube.com/watch?v=mEnjz2Gpjyk>
Metcon
Metcon (Time)
4 RFT
10 med ball thrusters
10 renegade rows
40s hollow hold
stretching 5-10 mins (No Measure)
hip external rotation stretch 1 min each leg
ankle mobility x 30s/leg
primal squat 30s
finish by rolling out/stretch tight areas***

## MON SEPTEMBER 10, 2018 WEEK 1 DAY 1

Bench Day Warm Up
SMR 5 mins
5 min AMRAP
-5 banded goodmornings
-10 pass throughs
-5 lat pull downs
-5 pinch and press
5 mins Skill Work: Barbell Bench Press (No Measure)
5 reps EMPTY bar focusing on cues  alternating with partner
Set up:
FEET
-in position where they can be "nailed" into the ground which will enable you to press down on heels to activate glutes
EYES
-should be in line with the bar when laying on the bench
HANDS
-placement of the hands should be a little bit wider than shoulder width apart
BACK
-should be tight creating an arch in your lower back
When performing the press:
Steps:(assuming you have proper position)
1.make sure heels are pressed into ground and your back is tight before you unrack the weight(MAINTAIN TIGHTNESS THROUGHOUT MOVEMENT; STABILITY IS KEY TO BEST PERFORMANCE FOR BENCH PRESS)
2.lower bar right below pecs keeping the elbows in(breathing through stomach holding it until bar reaches chest and exhale on the way up)
3.bar should finish right in front of eyes
A: Bench Press (4 x 10(31x1)8 RPE rest 2 mins between sets)
B1: Sorenson Hold w/ Hands OH (3 x 20-30s)
B2: Stir The Pot (3 x 8 rotations each way)
<https://www.youtube.com/watch?v=8CKMKjqTNE4>
Metcon
Metcon (No Measure)
10 min AMRAP
-20m bear crawl
-10 KB DL
-30s bike(hard!)
Cool-Down
stretching 5-10 mins (No Measure)
hip external rotation stretch 1 min each leg
ankle mobility x 30s/leg
primal squat 30s
finish by rolling out/stretch tight areas***

## SAT SEPTEMBER 08, 2018 LAST TEST DAY

**cardio Warm Up
SMR 5-10 mins
Bike or Row 3 mins
1 Round
Down and Back
-high knees
-side shuffle
-bear crawl
-inchworm
10 min Row MAX Calories (Calories)
burn as many calories as possible!!
Stretch
5 min stretch and breathe

## THU SEPTEMBER 20, 2018 WEEK 2 DAY 4

Deadlift Day Warm Up
5 mins SMR
5 min AMRAP
-8 KB DL
-5 Push Ups
-20s Plank
-20s Cobra Pose
5 min Skill Work: Barbell Deadlift (No Measure)
5 reps with 15s on bar focusing on cues  alternating with partner
Set up:
FEET
-put in a position that you feel is strongest; ideally a position that you can initiate the strongest pull with your legs; if this is the first time start feet hip width apart and test it out
HANDS
-placement of the hands should be outside of the knees enough for the arms to have a clear path for the pull without the knees getting in the way
BAR
-should be gently touching the shins(will allow you to use legs more)
BACK
-should be tight creating an arch in your lower back; tighten the lats
When performing the deadlift:
Steps:(assuming you have proper position)
1.tighten glutes and back(like described in setup
2.as you pull keep the bar close to your legs
3.when the bar reaches your knees push your hips forward
4.after lockout push your hips back keeping the bar close to the legs as you lower the bar to the floor

**if having trouble with set up then elevate the bar putting one 45lb plate down on each side of the barbell
A: Deadlift (4 x 10*8 RPE 2 min rest after each set)
2s pause @ knees on way down and 2s pause on floor after rep is completed
B: Close-Grip Floor Press (4 x 6(3s pause on floor after each rep))
1 min rest after each set
Metcon
Metcon (AMRAP - Reps)
8 min AMRAP
Push ups x max reps
every time you fail you have to do 12 med ball slams
***count reps
stretching 5-10 mins (No Measure)
hip external rotation stretch 1 min each leg
ankle mobility x 30s/leg
primal squat 30s
finish by rolling out/stretch tight areas***

## WED SEPTEMBER 19, 2018 WEEK 2 DAY 3

**cardio Warm Up
SMR 5-10 mins
Bike or Row 3 mins
1 Round
Down and Back
-high knees
-side shuffle
-bear crawl
-inchworm
Skill Work 5 mins
DBL KB Complex
Metcon (No Measure)
25 min AMRAP
-6 DBL KB Complex
-60 single unders
-30s side plank/side
-10 cal bike
HR @ yellow zone

## 19 DEC 2018 WEDNESDAY

### Metcon: "Dumb and Dumburp"

* E4min, 5x
* 9 burpee box jump-overs (24/20")
* 15 DB front squats (50s/35s#)
* 15/12 cal bike or row

#### Virtuosity training

3 untimed, unbroken sets of

* 10 good mornings (weighted)
* 20 box step-ups
* 30 glute bridges (banded or weighted)
* 40 sit-ups

## WED DECEMBER 26, 2018 DAY 1

### Full Body Warm Up

SMR 5 mins

AMRAP 6 mins

* 5 Push Ups
* 10 Dowel Good Mornings
* 10  Air Squats
* 10 Ring Rows
* A1: DB Bench Press (4 x 10(31x1))
* A2: Alternating Hamstring Curls on Rings (4 x 12(total))
* A3: Bird Dog Hold  (4 x 30s/side)
  * Extend opposite arm and opposite leg. Keep a level back and tighten core.
  * <https://www.youtube.com/watch?v=GwBprD9TLP4>
* 30s Rest

### Metcon (AMRAP - Rounds)

* 12 min Circuit
* 8 Squats with heart(using plate)
* 5 Pull ups(strict)
* 10 cal bike

## December 27, 2018

Strength: front or back squat
1 RM (pick one)
Metcon: "Cal Me Drogo"
AMRAP in 15 min of
27/18 cal bike (row, or 150 double-unders)
21 weighted sit-ups
15 chest-to-bar pull-ups
9 power cleans (135/80#)

## DEC 26 WEDNESDAY

Circuit strength:

3 rounds of
10 power cleans
10 front squats
10 push jerks
Round 1 @185/135#, Rd 2 @155/105#, Rd 3 @135/95#
Unbroken sets, 3 min rest between sets
Metcon: "Left-Over Schweddy's"
3 rounds (with a 15-min cap) of
10 power snatches (115/80#)
20 box jump-overs (24/20")
30 wall balls (20/14#)

## DEC 23 SUNDAY

Metcon: "Barbara"
5 rounds of
20 pull-ups
30 push-ups
40 sit-ups
50 air squats

---- 3 min rest ----

DEC
22
SATURDAY
Strength: 3 clean and 1 jerk complex

1 high hang clean
1 hang clean
1 power clean
1 split jerk

(build to heavy)

Metcon: "Hide Row Power"
30 power snatches (75/55#)
800m row (or 1.4/1.2 mi bike or 60 box step-ups)
60 thrusters
800m row (or 1.4/1.2 mi bike or 60 box step-ups)
30 power snatches

THU DECEMBER 27, 2018 DAY 2
Full Body Warm Up
SMR 5 mins
AMRAP 6 mins
5 Push Ups
10 Dowel Good Mornings
10  Air Squats
10 Ring Rows
A1: 1.5 rep Goblet Squats (4 x 8)
A2: Single Arm Row (4 x 10/arm)
<https://www.youtube.com/watch?v=pYcpY20QaE8>
A3: Side Plank (4 x 45s/side)
Rest 30s
Metcon (AMRAP - Rounds)
12 min Circuit
-DB Shoulder Press x 8
-Superman x 30s
-Row 10 cals

WED DECEMBER 26, 2018 DAY 1
Full Body Warm Up
SMR 5 mins
AMRAP 6 mins
5 Push Ups
10 Dowel Good Mornings
10  Air Squats
10 Ring Rows
A1: DB Bench Press (4 x 10(31x1))
A2: Alternating Hamstring Curls on Rings (4 x 12(total))
A3: Bird Dog Hold  (4 x 30s/side)
Extend opposite arm and opposite leg. Keep a level back and tighten core.

<https://www.youtube.com/watch?v=GwBprD9TLP4>
30s Rest
Metcon (AMRAP - Rounds)
12 min Circuit
8 Squats with heart(using plate)
5 Pull ups(strict)
10 cal bike

MON DECEMBER 24, 2018 12 DAYS OF CHRISTMAS!
**cardio Warm Up
SMR 5-10 mins
Bike or Row 3 mins
1 Round
Down and Back
-high knees
-side shuffle
-bear crawl
-inchworm
Dirigo's 12 Days of Christmas 2018 (Time)
10m Inchworm
2 Burpees
30s Plank
4 KB Cleans
5 MBS
6 Knees to Chest
7 Wall Balls
8 Ring Rows
9 Push ups
10 Walking Lunges
11 Cal Bike
12 DB Thrusters

## FRI DECEMBER 21, 2018 TEST DAY 5

### Sweet Aero Warm-Up

Warmup:
Row/Bike/Run 5 -10 Min - You Choose. Get Blood Moving
Wrist Circles 10-15 each direction
Elbow Circles 10-15 each direction
Arm Circles 10-15 each direction
Arm Rotations 10-15 each direction
Tricep Stretch Side Bends 10-15 each direction
Torso Rotations 10-15 each direction
Hip Circles 10-15 each direction
Walking Lunge and Twist 10-15 each leg
Side Lunge 10-15 each leg

Row or AD Below

2 Rounds
10 sec HARD
Walk 90 sec
A: Burpee Repeat Test (4 Rounds for reps)
40 seconds of max repetitions of burpees, 20 seconds of rest

repeat 4 times
B: Sorensen hold (Max Time)
Arms Across Chest
Press Heels into Metal
Squeeze Butt as Tight as You Can
Hold at Parallel as Long as Possible

HU DECEMBER 20, 2018 TEST DAY 4
*Deadlift Warm-Up*
Row 300m

10 toe elevated toe touches
10 heel elevated toe touches
10 half kneeling hip mobilization each side

10 ring rows
20 bridges
20 clam shells
20m lateral walk
20m monster walk
10 x Dorian Deadlifts(bar only)
A: Deadlift (1 RM)
1st set:60% x 6
2nd set:70% x 3
3rd set:80% x 2
4th set 90% x 1
5th set 100% x 1
6th set 100%+ x 1
rest 2 mins in between sets for first 4 sets; then every set after that rest 5 mins or more depending on how you feel

## WED DECEMBER 19, 2018 TEST DAY 3

### Sweet Aero Warm-Up *

Warmup:
Row/Bike/Run 5 -10 Min - You Choose. Get Blood Moving
Wrist Circles 10-15 each direction
Elbow Circles 10-15 each direction
Arm Circles 10-15 each direction
Arm Rotations 10-15 each direction
Tricep Stretch Side Bends 10-15 each direction
Torso Rotations 10-15 each direction
Hip Circles 10-15 each direction
Walking Lunge and Twist 10-15 each leg
Side Lunge 10-15 each leg

Row or AD Below

2 Rounds
10 sec HARD
Walk 90 sec
Metcon (No Measure)
Recovery AMRAP 40 mins
-20 cal Row
-30s Childs Pose
-20 cal Bike
-30s Cobra Pose
-10 Downward Dogs
Green and Blue Zone ONLY(60-79% Max HR)

## TUE DECEMBER 18, 2018 TEST DAY 2

### Shoulder Day

SMR: 3-5 minutes

15 quadruped t-spine rotations each side

20 Pass Throughs / 10 ATW with bands
20 Pinch & Press(from pike position)
8 Ext Rotations
8 Full Can

2 Rounds
10 Bottoms Up press(5 per side)
8 Halos
30s Hollow hold
A: Bench Press (1 RM(20x0))
start with empty bar for 10 reps
1st set:60% x 6
2nd set:70% x 3
3rd set:80% x 2
4th set 90% x 1
5th set 100% x 1
6th set 100%+ x 1
rest 2 mins in between sets for first 4 sets; then every set after that rest 5 mins or more depending on how you feel
2k Row (Time)
Max Effort 2k Row

MON DECEMBER 17, 2018 TEST DAY 1
*Back Squat Warm-Up*
Foam Roll 3-5min

10 half kneeling hip mobilizations each side
10 prayer stretches
10 t-spine rotations on foam roller
30s/arm Front Rack Mobility Stretch

10 leg lowers on rig/leg
20 Scorpions
20 Alt arm /legs Bird Dog

1 Round
20m Lateral Walk
20 Bridges NO Weight
20 Banded Good mornings
A: Back Squat (1 RM(20x0))
start with empty bar for 10 reps
1st set:60% x 6
2nd set:70% x 3
3rd set:80% x 2
4th set 90% x 1
5th set 100% x 1
6th set 100%+ x 1
rest 2 mins in between sets for first 4 sets; then every set after that rest 5 mins or more depending on how you feel
B: Plank Hold (Right Side)
C: Plank Hold (Left Side)

SAT DECEMBER 15, 2018 DELOAD WEEK DAY 4
**cardio Warm Up
SMR 5-10 mins
Bike or Row 3 mins
1 Round
Down and Back
-high knees
-side shuffle
-bear crawl
-inchworm
Metcon (No Measure)
30 mins RECOVERY AMRAP
-20 cals bike(easy pace)
-8 Rolling Cossacks/leg
-20 cal row(easy pace)
-10 hip mobilizations/leg
-10 RDLs(bar only)

WED DECEMBER 12, 2018 DELOAD WEEK DAY 3
Deadlift Day Warm Up
5 mins SMR
5 min AMRAP
-8 KB DL
-5 Push Ups
-20s Plank
-20s Cobra Pose
5 min Skill Work: Barbell Deadlift (No Measure)
5 reps with 15s on bar focusing on cues  alternating with partner
Set up:
FEET
-put in a position that you feel is strongest; ideally a position that you can initiate the strongest pull with your legs; if this is the first time start feet hip width apart and test it out
HANDS
-placement of the hands should be outside of the knees enough for the arms to have a clear path for the pull without the knees getting in the way
BAR
-should be gently touching the shins(will allow you to use legs more)
BACK
-should be tight creating an arch in your lower back; tighten the lats
When performing the deadlift:
Steps:(assuming you have proper position)
1.tighten glutes and back(like described in setup
2.as you pull keep the bar close to your legs
3.when the bar reaches your knees push your hips forward
4.after lockout push your hips back keeping the bar close to the legs as you lower the bar to the floor

**if having trouble with set up then elevate the bar putting one 45lb plate down on each side of the barbell
A: Deadlift (5 x 5/3/2/2/1)
6 RPE = whatever weight you choose you should be able to do 4 more reps at that rep range; focus on technique and explosiveness; Remember its DELOAD WEEK we want to recover!!
B: Landmine Press (Standing)  (4 x 6/arm)
<https://www.youtube.com/watch?v=jCfcGei-NqM>
1 min rest after each set
Metcon
Metcon (No Measure)
12 min EMOM
1: Tricep Dips x 10
2: Weighted bridge(DB) x 40s
3: burpees x 12
4: hollow hold x 35s
stretching 5-10 mins (No Measure)
stretch and foam roll tight areas***

## TUE DECEMBER 11, 2018 DELOAD WEEK DAY 2

### Squat Day Warm up K

5 mins SMR
5 min AMRAP
Goblet Squat x 4(w/ 10s pause at bottom)**use light KB
row x 200m
bear crawl x 20m
Skill Work 5 mins
A: Box Squat (Loaded) (5 x 5/3/2/2/1  )
<https://www.youtube.com/watch?v=mjm3W3tQejM>
6 RPE = whatever weight you choose you should be able to do 4 more reps at that rep range; focus on technique and explosiveness; Remember its DELOAD WEEK we want to recover!!
B: Pull-ups (3 x 8-10)

Metcon (No Measure)
10 min 30s ON 30s OFF
-reverse lunge to squat
-renegade rows
stretching 5-10 mins (No Measure)
stretch and foam roll tight areas***

## SAT DECEMBER 08, 2018 WEEK 5 DAY 6

### Cardio Warm Up M

SMR 5-10 mins
Bike or Row 3 mins
1 Round
Down and Back
-high knees
-side shuffle
-bear crawl
-inchworm
Metcon (Time)
Partner Elimination!
One partner works while the other is resting.
1.3,000m Row
2.50 burpees
3.50 Box Jumps or Step ups
4.50 MBS
5.3 min Plank
6.50 DB Floor Press(can use barbell if needed)
7.50 cal bike

## FRI DECEMBER 07, 2018 WEEK 5 DAY 5

### Squat Day Warm up

5 mins SMR
5 min AMRAP
Goblet Squat x 4(w/ 10s pause at bottom)**use light KB
row x 200m
bear crawl x 20m
A1: KB/DB Front Rack Squat (4 x 5(24x1))
-KB/DB in clean grip position
-barbell front squat technique applies to this
A2: KB OH Carry (4 x 20m(focus on stability/positioning;go SLOW)

Metcon (No Measure)
10 min AMRAP
Partner # 1: Bear Crawl 40m
Partner #2 : Wall sit until partner #1 is done
stretching 5-10 mins (No Measure)
stretch and foam roll tight areas***

## DEC 19 WEDNESDAY

### Metcon: "Dumb and Dumburp 2"

E4min, 5x
9 burpee box jump-overs (24/20")
15 DB front squats (50s/35s#)
15/12 cal bike or row

Virtuosity training:
3 untimed, unbroken sets of
10 good mornings (weighted)
20 box step-ups
30 glute bridges (banded or weighted)
40 sit-ups

## Feb 6 2019 3/4 Throwdown (5:00) and Core FUNction (5:45) WOD

### Strength circuit: E3min, 4x

* 5-10 bench presses
* 30 sec smash
* 15-sec row sprint
* 12 pause side jumps

### Metcon: “4 to Tango”

* AMRAP in 4 min of
* 8 pull-ups or ring rows
* 8 weighted lunges
* 8/8 one-arm DB push presses
* 8 weighted/anchored sit-ups

--- 1 min rest ----
3x

### Core cash-out (CFx only)

* 2x1 min each dry swim, banded kick-ins

Tyson Victor Weems
April 15 at 9:48 AM Tonight's 3/4 Throwdown (5 PM) and Core FUNction (5:45) WOD:

Strength/speed: 4 rounds in 10 min of
5 deadlifts or hang power cleans (or 10 KB swings)
10 pike-ups
10-second bike sprint

Metcon: “The Gathering Storm”
3 min of in 1 min
3 cals machine
3 DB push presses
3 box jumps
3 hang knee raises or v-ups (strict or kip)
Then 3 min of in 1 min 4 each
Then 3 min of in 1 min 5 each
Then 3 min of in 1 min 6 each

Core cash-out (CFx only): 3 sets of
10 erg roll-ins
20 Russian twists

April 8 at 9:00 AM Tonight's 3/4 Throwdown (5pm) and Core FUNction (5:45) WOD:

Strength: 3 sets in 10 min of
5 back squats
15-sec ski erg sprint or 5 hang power cleans
5-10 paralette dips or angled close push-ups
30 sec smash

Metcon: “90s Rap”
AMRAP in 90 sec each of
Cal bike
Double-unders, singles, or box steps
---- 1 min rest ----
AMRAP in 90 sec each of
Cal row
DB push presses
---- 1 min rest ----
AMRAP in 90 sec each of
Cal bike
Plate pushes
---- 1 min rest ----
AMRAP in 90 sec each of
Cal row
Anchored sit-ups or hang knee raises

Core cash-out (CFx only): kick-backs
30 sec/leg kneeling, standing

Tyson Victor Weems
 April 3 at 11:00 AM
Tonight's 3/4 Throwdown (5 pm) and Core FUNction (5:45) WOD:

Strength 1: 3 rounds in 6 minutes
5-10 bent-over barbell rows
3-5 DB shoulder presses (standing or on floor)

Strength 2: 3 rounds in 6 minutes
3-5 pull-ups (regular, negatives, or holds) or pull-downs
5 each side one-arm OH squats

Metcon: “All or Nothing”
30 sec on/off, alternating, 3x
Bike or row
Ball slams
Burpees
Box jumps or steps

Core cash-out (CFx only):
30 sec on/15 sec off
Plank swims
Side plank-throughs R
SPTs L
3x

April 1 at 12:44 PM Tonight's 3/4 Throwdown (5 PM) and Core FUNction (5:45) WOD:

Strength (13:00 – 23:00): 4 rounds in 10 minutes of
5 hang power cleans or 10 KB swings
3-10 dips (ring, Matador, paralette, or box)
20-30 sec hollow hold

Metcon: “Not Ghana Finnish”
30-20-10-20-30
(with a 15-min cap)
OH plate lunges
Machine cals
Hang knee raises or K2E
Plus 20 D-Us, tall singles, or plate hops after each set (300 total if getting all the reps)

Core cash-out (CFx only):
40 alternating leg bridge-ups (2-sec hold at top of each)

 is with Tj Leach and 2 others.
 March 27 at 10:04 AM
Tonight's 3/4 Throwdown (5 PM) and Core FUNction (5:45) WOD:

Strength: 3 rounds in 10 min of
10-15 back squats
20 pause side hops
10 pike-ups
30 sec smashing

Metcon 1: “Rip Tabs”
Consecutive Tabatas, 4 rounds each
Push-ups
Pull-ups (regular or jumping)
Sit-ups

Metcon 2: “The Kazakh Step”
AMRAP in 5 min of
10 barbell box step-ups
5 hang power cleans or bent-over rows
5 shoulder to overhead

Core Cash-out (CFx only): 30 sec each, 2x (4 min total)
Plank
Side plank L, R
Bridge hold

---

 is with Tj Leach and Bobby Cooper.
 March 25 at 7:50 AM
Today's 3/4 Throwdown (5 PM) and Core FUNction (5:45) WOD:

Strength: 4 sets in 10 min
5-10 supine rows
5-10 DB bench press
5-10 hollow outs

Metcon: “The Spice Trade”
In 6 min
40 cal row or bike
AMRAP in remaining time
20 hang knee raises
20 lunges
20 1-arm push presses
---- 2 min rest ----
In 6 min
20 box jump burpees
20 KB swings
20 goblet squats
AMRAP cal row or bike

Core cash-out (CFx only): alternating Tabata, 4x
Russian twists, side planks

March 20 at 10:45 AM The 5 PM 3/4 Throwdown and 5:45 Core FUNction WOD:

Strength: 4 rounds in 12 min of
5 deadlifts or hang power cleans (or 10 KB swings)
5-10 ring push-ups
10 pike-ups
30 sec smash

Metcon: “Sixy Sadist”
AMRAP in 1, 2, 4, 2, 1 min (WYLO, w/ 1-min rests between each)
12 dips
6 pull-ups
12 goblet lunges
6 plank burpees
12 anchored sit-ups
6 plate pushes

Core cash-out: 1 min each, 2-3x
Banded side steps
Hollow hold max accumulation

 is with Meghan Blaszczyk.
 March 4
Tonight's 3/4 Throwdown (5:00) and Core FUNction (5:45) WOD:

Strength: 4 rounds (one light) in 12 min of
5 strict barbell presses
5-10 each side bent-over DB row
30-45 sec hollow-outs or planks

Metcon: “Snow What”
Consecutive ½ Tabata (2 min each)
Ring push-ups
Cal row
Box jumps
KB swings
Lunges
Plank burpees

Core cash-out: 3 rounds of
8-12 erg roll-ins or bridge-ups
12/12 side plank-throughs

 is with Sean Welch and David Collins.
 February 25
Tonight's 3/4 Throwdown (5:00) and Core FUNction (5:45) WOD:

Strength:
8-12 back squats
8-12 straddle-ups
30 sec smashing

Metcon: “The Old 5 and 10”
AMRAP in 15 min of
10 KB swings
5/5 single DB push presses
10 hang knee raises
5 plate pushes
10 box step- or jump-overs
Plus 10 mountain climbers E2min

Core cash-out: 3 sets
12/12 kneeling banded kick-backs

February 20 Tonight's 3/4 Throwdown (5:00) and Core FUNction (5:45) WOD:

Strength 1: 3 rounds in 6 min
8/8 DB bent-over rows
8-12 hollow-outs

Strength 2: 3 rounds in 6 min
8-12 dips
8-12 erg roll-ins or 8/8 single-leg bridges

Metcon: “Alter ErgOH”
3 rounds (30 sec on/off, alternating) of
Jump rope or box jump-overs or high knees
Bike (or row, ski erg, or plate push)
KB swings
OH plate lunges

Core Cash-out (CFx only): alternating Tabata (3x)
Russian twists
Supermans

 is with Tori Anderson and Dave Norman.
 February 18
Tonight's 3/4 Throwdown (5:00) and Core FUNction (5:45) WOD:

Strength: 4 rounds in 12 min of
8-12 seated DB presses
4-8 ring rows or pull-ups
10 pike-ups
30-45 sec smashing

Metcon: “Up for the Count”
AMRAP in 4 min of
4 ball slams
4 cal bike
Then 8 each, 12 each…
---- 1 min rest ----

* AMRAP in 4 min of
* 4 farmer box step-ups
* 4 v-ups or weighted/anchored sit-ups
  Then 8s, 12s…
  ---- 1 min rest ----
  AMRAP in 4 min of
* 4 cal row or ski
* 4 plank burpees
* Then 8s, 12s…

Core cash-out (CFx only):

* 3 sets of 12/12 banded kick-backs
  
  is with Ben Havey.
  February 13
  Tonight's 3/4 Throwdown (5 PM) and Core FUNction (5:45) WOD:

Strength: 4 rounds in 12 min of

* 5-10 back squats
* 5-10 dips
* 30-45 sec smashing

Metcon: “Been a Minute”
(each 2 min on, 1 min rest)

1. 20/15 cal bike
   AMRAP KB swings
2. 20/15 cal row
   AMRAP box jumps
3. 20/15 cal bike
   AMRAP v-ups or hang knee raises
4. 20/15 cal row
   AMRAP DB push presses

Core cash-out (CFx only):

* 1 min dry swim
  Then 30 sec each
  Side plank-throughs R

* SP-Ts L

* Hollow hold or plank
  
  is with Lyndsey McDonald and 2 others.
  February 11
  Tonight's 3/4 Throwdown (5 PM) and Core FUNction (5:45) WODs:

Speed: alternating OTM, 5x
10-sec max effort bike, plate push, or ski

Metcon: “Jump In the Line”
(with a 20-min cap)

* 4 ring rows or pull-ups
  6 push-ups
  8 lunges
  10 anchored/weighted sit-ups
  20 jumps (rope, plate, or line)

Core cash-out (CFx only): 3 rounds of

* 30 sec erg roll-ins

* 30 sec banded side steps
  
  is with Sheila Sawyer and Lisa Barcellona-DiBiase.
  February 4

## Tonight's 3/4 Throwdown (5:00) and Core FUNction (5:45) WOD:

### Strength circuit: E3min, 4x 2/4

* 5-10 bench presses
* 30 sec smash
* 15-sec row sprint
* 12 pause side jumps

### Metcon: “4 to Tango” 2/4

AMRAP in 4 min of

* 8 pull-ups or ring rows
* 8 weighted lunges
* 8/8 one-arm DB push presses
* 8 weighted/anchored sit-ups

--- 1 min rest ----
3x above

### Core cash-out 2/4

* 2x1 min each dry swim, banded kick-ins

## January 30 Tonight's 3/4 Throwdown and Core FUNction WOD

### Strength Circuit: 4 rounds in 12 minutes

* 3-5 back squats
* 10 pike-ups
* 8-12 dips

### Metcon 1: “Blue Chip”

* (with a 7-minute cap)
* 500m row
* 40 air squats
* 30 sit-ups
* 20 push-ups
* 10 pull-ups

### Metcon 2: “Petal Mettle”

* (in teams of 3)
* AMCAP bike in 6 min

### Core Cash-Out 1/30

* Kneeling banded kick-backs 12/12 x 3

## January 28 Tonight's 3/4 Throwdown

### Strength circuit 1/28

* 10-15 seated DB presses
* 30-45 sec of smashing calves, upper back
* 10 cal ski erg
* 10/10 one-arm bent-over rows

### Metcon: “Line Jumper”

* AMRAP in 12 min of
* 10 farmer box step-ups
* 10 plank burpees
* 10 goblet squats
* 10 ring rows or pull-ups
* 40 line hops or j rope singles (or 20 doubles)

### Core cash-out 1/28

* 2 sets of 30 sec each of
* V-ups
* Dry swim
* Russian twists

## January 23 The 5 PM 3/4 Throwdown and 5:45 PM Core FUNction WOD

### Speed: 12-second bike sprint OTM, 5x

### Strength: 4 rounds in 12 minutes of

* 5-10 dips
* 5-10 deadlifts or squats
* 30-45 sec smash

### Metcon: “Smack Down”

* AMRAP in 15 min of
* 60 double-unders or plate hops
* 50 cal row
* 40 DB push presses
* 30 KB swings
* 20 burpees
* 10 plate pushes

### Core cash-out

* plank swims 2 minute accumulated, Superman rows 1 minute

## January 21 Tonight's 3/4 Throwdown

### Strength circuit: 4 rounds in 12 minutes

* 5-10 strict DB presses
* 5-10 ring rows
* 5-10 hollow-outs
* 10 weighted step-ups

### Metcon: “The Moral Universe”

* (4 Tabata sets each, 2 rounds)
* Ball slams
* Bike
* Farmer carries
* Box jumps

### Core cash-out 1/21

* 10/10 side plank-ups
* 12/12 banded kick-backs

## January 16 Tonight's 3/4 Throwdown and Core FUNction WOD

### Strength 1: 3 rounds in 4 minutes of

* 10 jumping pull-ups or bent-over rows
* 5-10 hollow-outs

### Strength 2: 4 rounds in 8 minutes of

* 5-10 back squats
* 5-10 one-arm DB bench presses

### Metcon: “Not on the List”

(AMRAPs starting WYLO)

* Goblet squats

* KB swings

* KB deadlift burpees

* Farmer box step-ups

* Russian twists (each side)

* Dips

* AMRAP in 30 sec of 4 each
  ---- 30 sec rest ----

* AMRAP in 1 min of 8 each
  ---- 30 sec rest ----

* AMRAP in 2 min of 12 each
  ---- 1 min rest ----

* AMRAP in 4 min of 16 each
  ---- 1.5 min rest ----

* AMRAP in 6 min of 20 each

### Core cash-out: for 5 minutes

* Banded kick-backs/-ins
