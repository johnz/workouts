# March 2021 Weekly Routines

## March 2021

Crossover Symmetry every day

### Mondays Total Body Pushing - 400 Cals

Warmup - cardio 5 minutes, stretching 5 minutes
Workout 40 minutes

- A1: Standing dumbell overhead press <https://youtu.be/M2rwvNhTOu0>
  - 12 Reps, 4 sets, 3111 tempo, 30 secs rest
- A2: Inchworms <https://youtu.be/t6EjIe4A-oI>
  - 10 reps, 4 sets, 30 secs rest
- B1: Bench Press - from ground or bench
  - 12 reps, 4 sets, 3111 tempo, 30 secs rest
- B2: Shoulder Taps <https://youtu.be/DZ460QK_gPk>
  - 12 reps/side, 4 sets, 30 sec rest
- C1: Back Squat (or dumb bell squat)
  - 12 reps, 4 sets, 3111 tempo, 30 secs rest
- C2: Wall Sit: perfect 90
  - 30 - 60 secs work, 4 sets, 30 secs rest

### Wednesday Total Body Pulling - 400 Cals

Warmup - cardio 5 minutes, stretching 5 minutes
Workout 40 minutes

- A1: Pullups, Band assisted if needed
  - 12 reps, 4 sets, 3111 tempo, 30 secs rest
- A2: Cliffhanger Plank Walks <https://youtu.be/xEFa_fbMz24>
  - 10 reps, 4 sets, 30 secs rest
- B1: Bent over barbell row
  - 12 reps, 4 sets, 30 secs rest
- B2: Plank Hip Taps <https://youtu.be/wU70zWxX7IA>
  - 10 reps/side, 4 sets, 30 secs rest
- C1: Deadlift
  - 12 reps, 4 sets, 30x1 tempo, 30 secs rest
- C2: Hamstring Long Plank <https://youtu.be/3at9ULJrx1U>
  - 30 sec hold, 4 sets, 30 rest

### Friday Circuit - 40 minutes, Target 5 rounds - 450 Cals

Warmup - cardio 5 minutes, stretching 5 minutes
Workout 40 minutes

- A1: Kettlebell swings - 12 reps
- A2: Medicine ball slams - 12 reps
- A3: Push up - 12 reps
- A4: Burpee, controlled - 12 reps
- A5: Sit-up, Slow - 12 reps

### Saturday 1 "Fatal 40, reduced" benchmark WOD - 500 Cals

Warmup - cardio 5 minutes, stretching 5 minutes
Workout 50 minutes - Zone 4 cardio

- Buy in: 10 minute row/jog
- 40 - Ball thrusters (20/12 lb, or dumb bells) <https://youtu.be/u3wKkZjE8QM>
- 40 - Hang cleans (95/65 lb, or dumb bells) <https://youtu.be/SYxObzJ3gn0>
- 200 meter row/jog, recovery
- 40 - Pull ups
- 40 - Deadlifts (95/65 lb, or dumb bells) <https://youtu.be/JNpUNRPQkAk>
- 200 meter row/jog, recovery
- 40 - Push ups
- 40 - Box jumps, 3-stair step ups <https://youtu.be/NBY9-kTuHEk> or <https://youtu.be/5qjqDHOUh-A>
- 200 meter row/jog, recovery
- 40 - Kettle bell swings (53/35 lbs) <https://youtu.be/mKDIuUbH94Q>
- 40 - Toes to wall/candle sticks  <https://youtu.be/WXoNNx-uOtU>
- 200 meter row/jog, recovery
- 40 - Air squats
- 40 - Snatches (35/20 lb dumb bells) <https://youtu.be/3mlhF3dptAo>
- 200 meter row/jog, recovery
- 40 - Jumping jax
- 40 - Sit-ups
- 200 meter row/jog, recovery
- 40 - Burpees <https://youtu.be/auBLPXO8Fww>
- Cash out: stretching

### Saturday 2 "Jelly Beans" - 400 Cals

Warmup - cardio 5 minutes, stretching 5 minutes
Workout 50 minutes - Zone 4 cardio
1 rounds for time

* 50 Rolling situps
* 30 Push ups
* 30 Toes to wall/rig, candlesticks
* 30 Dumbell thrusters
* 10 Weighted lunges, each side
* 2 minutes plank, choice
* 10 Man makers, or scale to renegade rows
* Cash out: stretching
